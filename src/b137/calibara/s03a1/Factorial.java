package b137.calibara.s03a1;

public class Factorial {
    public static void main(String[] args) {
        System.out.println("Factorial\n");
        int i,fact=1;
        int number=7;
        for(i=1;i<=number;i++){
            fact=fact*i;
        }
        System.out.println("Factorial of "+number+" is: "+fact);
    }
}
